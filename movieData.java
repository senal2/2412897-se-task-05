package movie;

import java.util.Arrays;
import java.util.List;

public class movieData {

    public static class Movie {
        private final String title;
        private final String genre;
        private final int releaseYear;
        private final String director;
        private final int duration; // in minutes

        public Movie(String title, String genre, int releaseYear, String director, int duration) {
            this.title = title;
            this.genre = genre;
            this.releaseYear = releaseYear;
            this.director = director;
            this.duration = duration;
        }

        public String getTitle() {
            return title;
        }

        public String getGenre() {
            return genre;
        }

        public int getReleaseYear() {
            return releaseYear;
        }

        public String getDirector() {
            return director;
        }

        public int getDuration() {
            return duration;
        }

        @Override
        public String toString() {
            return String.format("\n\"Title: %s\n\"Genre: %s\n\"Release Year: %d\n\"Director: %s\n\"Duration: %d minutes\n",
                    title, genre, releaseYear, director, duration);
        }
    }

    public static class MovieCollection {
        List<Movie> movies;

        public MovieCollection(Movie... movies) {
            this.movies = Arrays.asList(movies);
        }
    }
}

