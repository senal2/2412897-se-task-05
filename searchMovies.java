package movie;

import java.util.List;
import java.util.Scanner;



public class searchMovies {

    private static String choice;

    public static void main(String[] args) {
        System.out.println("Enter your choice");
        Scanner scanner = new Scanner(System.in);
        choice = scanner.nextLine();
        if (choice != null) {
            dataSet dataSet = new dataSet(); 
            List<movieData.Movie> movieList = movieFilter.filterByModelName(dataSet.dataSet, choice);
            List<movieData.Movie> parallelMovieList = movieFilter.filterWithParallelStream(dataSet.dataSet, choice);
            List<movieData.Movie> sequentialMovieList = movieFilter.filterWithSequentialStream(dataSet.dataSet, choice);

            System.out.println("Your choice is:");
            printMovieDetails(movieList);

            System.out.println("Parallel Stream choice is:");
            printMovieDetails(parallelMovieList);

            System.out.println("Sequential Stream choice is:");
            printMovieDetails(sequentialMovieList);
        }
    }

    private static void printMovieDetails(List<movieData.Movie> movies) {
        for (movieData.Movie movie : movies) {
            System.out.println(movie);
        }
        System.out.println();
    }
}

