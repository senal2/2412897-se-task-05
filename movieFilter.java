package movie;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import movie.movieData.Movie;

public class movieFilter {

    public static List<movieData.Movie> filterByModelName(List<movieData.Movie> movies, String modelName) {
//        return movies.stream()
//                .filter(movie -> movie.getTitle().contains(modelName) ||
//                        movie.getGenre().contains(modelName) ||
//                        String.valueOf(movie.getReleaseYear()).contains(modelName) ||
//                        movie.getDirector().contains(modelName))
//                .collect(Collectors.toList());
    	
    	if (movies == null) {
            return Collections.emptyList();
        } else {
            return movies.stream()
                    .filter(movie -> movie.getTitle().contains(modelName) ||
                            movie.getGenre().contains(modelName) ||
                            String.valueOf(movie.getReleaseYear()).contains(modelName) ||
                            movie.getDirector().contains(modelName))
                    .collect(Collectors.toList());
        }
    }

    public static List<movieData.Movie> filterWithParallelStream(List<movieData.Movie> movies, String modelName) {
        return movies.parallelStream()
                .filter(movie -> movie.getTitle().startsWith(modelName) ||
                        movie.getGenre().endsWith(modelName) ||
                        String.valueOf(movie.getReleaseYear()).endsWith(modelName) ||
                        movie.getDirector().contains(modelName) ||
                        movie.getTitle().matches(modelName))
                .collect(Collectors.toList());
    }

    public static List<movieData.Movie> filterWithSequentialStream(List<movieData.Movie> movies, String modelName) {
        return movies.stream()
                .filter(movie -> movie.getTitle().startsWith(modelName) ||
                        movie.getGenre().endsWith(modelName) ||
                        String.valueOf(movie.getReleaseYear()).endsWith(modelName) ||
                        movie.getDirector().contains(modelName) ||
                        movie.getTitle().matches(modelName))
                .collect(Collectors.toList());
    }

	public static void printMovieDetails(List<Movie> movieList) {
		// TODO Auto-generated method stub
		
	}
}
