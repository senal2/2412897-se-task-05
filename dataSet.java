package movie;

import java.util.Arrays;
import java.util.List;

import movie.movieData.Movie;

public class dataSet {

    public static List<movieData.Movie> createMovieList() {
        return Arrays.asList(
                new movieData.Movie("Interstellar", "Sci-Fi", 2014, "Christopher Nolan", 169),
                new movieData.Movie("The Godfather", "Crime", 1972, "Francis Ford Coppola", 175),
                new movieData.Movie("The Matrix", "Action", 1999, "The Wachowskis", 136),
                new movieData.Movie("Schindler's List", "Biography", 1993, "Steven Spielberg", 195),
                new movieData.Movie("The Shawshank Redemption", "Drama", 1994, "Frank Darabont", 142)
        );
    }

	public List<Movie> movieData;
	public List<Movie> dataSet;
	
	public dataSet() {
        // Initialize the dataSet with the movie list
        dataSet = createMovieList();
    }
}

